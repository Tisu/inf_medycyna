﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Info_medycyna.Class.Structs;

namespace Info_medycyna.Class
{
    class ReadData
    {
        private int m_indexP = 0;
        private int m_line_length = 0;

        PatientData[] m_patient_data;
        public ReadData(string a_data)
        {
            string[] split_buffor = a_data.Split('\t');
            m_line_length = split_buffor.Length / 59 + 1;
            m_patient_data = new PatientData[m_line_length];

            for (int i = 0; i < m_line_length - 1; i++)
            {
                m_patient_data[i] = new PatientData();
                IndexGenerator index_generator = new IndexGenerator(i, m_line_length);
                m_patient_data[i].m_general.m_age = int.Parse(split_buffor[index_generator.GetIndex()]);
                m_patient_data[i].m_general.m_sex = (eSex)int.Parse(split_buffor[index_generator.GetIndex()]);
                m_patient_data[i].m_pain.m_pain_location = (ePainLocation)int.Parse(split_buffor[index_generator.GetIndex()]);
                m_patient_data[i].m_pain.m_pain_chest_radiation = (ePainChestRadiation)int.Parse(split_buffor[index_generator.GetIndex()]);
                m_patient_data[i].m_pain.m_pain_character = (ePainCharacter)int.Parse(split_buffor[index_generator.GetIndex()]);
                m_patient_data[i].m_pain.m_onset_of_pain = (eOnsetOfPain)int.Parse(split_buffor[index_generator.GetIndex()]);
                m_patient_data[i].m_pain.m_number_of_hours_since_onset = int.Parse(split_buffor[index_generator.GetIndex()]);
                m_patient_data[i].m_pain.m_duration_of_last_episode = (eDurationOfTheLastEpisode)int.Parse(split_buffor[index_generator.GetIndex()]);

                m_patient_data[i].m_associated_symptoms.m_nausea = int.Parse(split_buffor[index_generator.GetIndex()]) == 0 ? false : true;
                m_patient_data[i].m_associated_symptoms.m_diaphoresis = int.Parse(split_buffor[index_generator.GetIndex()]) == 0 ? false : true;
                m_patient_data[i].m_associated_symptoms.m_palpitations = int.Parse(split_buffor[index_generator.GetIndex()]) == 0 ? false : true;
                m_patient_data[i].m_associated_symptoms.m_dyspnea = int.Parse(split_buffor[index_generator.GetIndex()]) == 0 ? false : true;
                m_patient_data[i].m_associated_symptoms.m_diziness_syncope = int.Parse(split_buffor[index_generator.GetIndex()]) == 0 ? false : true;
                m_patient_data[i].m_associated_symptoms.m_burping = int.Parse(split_buffor[index_generator.GetIndex()]) == 0 ? false : true;
                m_patient_data[i].m_associated_symptoms.m_palliative_factors = (ePalliativeFactors)int.Parse(split_buffor[index_generator.GetIndex()]);

                //m_patient_data[i].m_current_medical_usage.m = int.Parse(split_buffor[index_generator.GetIndex()]) == 0 ? false : true;
                //m_patient_data[i].m_associated_symptoms.m_nausea = int.Parse(split_buffor[index_generator.GetIndex()]) == 0 ? false : true;
                //m_patient_data[i].m_associated_symptoms.m_nausea = int.Parse(split_buffor[index_generator.GetIndex()]) == 0 ? false : true;

            }
           split_buffor[m_line_length-2].TrimEnd(new char[]{'\t','\n'});
           bool a = true;
        }
    }
}
