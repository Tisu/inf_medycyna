﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Info_medycyna.Class.Structs
{
    public enum ePalliativeFactors
    {
        none,
        relieved_by_nitroglycerin_within_5_min,
        Relieved_by_nitroglycerin_after_more_than_5_min,
        Antacids,
        Analgesia_except_morphine,
        Morphine,
    }
    struct sAssociatedSymptoms
    {
        public bool m_nausea;
        public bool m_diaphoresis;
        public bool m_palpitations;
        public bool m_dyspnea;
        public bool m_diziness_syncope;
        public bool m_burping;
        public ePalliativeFactors m_palliative_factors;
    }
}
