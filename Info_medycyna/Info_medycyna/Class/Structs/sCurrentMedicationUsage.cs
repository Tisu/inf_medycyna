﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Info_medycyna.Class.Structs
{
    struct sCurrentMedicationUsage
    {
        public bool m_diuretics;
        public bool m_nitrates;
        public bool m_beta_blockers;
        public bool m_digitalis;
        public bool m_nonsteroidal_anti_inflammatory;
        public bool m_antacids_h2_blockers;

    }
}
