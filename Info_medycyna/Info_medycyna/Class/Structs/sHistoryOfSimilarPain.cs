﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Info_medycyna.Class.Structs
{
    struct sHistoryOfSimilarPain
    {
        public bool m_prior_chest_pain_of_this_type;
        public bool m_physician_consulted_for_prior_pain;
        public bool m_prior_pain_related_to_heart;
        public bool m_prior_pain_due_to_MI;
        public bool m_prior_pain_due_to_angina_prectoris;
    }
}
