﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Info_medycyna.Class.Structs
{
    public enum ePainLocation
    {
        retrosternal,
        left_precordial,
        right_precordial,
        left_lateral_chest,
        right_lateral_chest,
        abdominal,
        back,
        other
    }
    public enum ePainChestRadiation
    {
        neck,
        jaw,
        left_shoulder,
        left_arm,
        right_shoulder,
        back,
        abdomen,
        other,
    }

    public enum ePainCharacter
    {
        constant,
        episodic,
        episodic_than_constant,
        constant_than_episodic,
        dull_pressure,
        sharp,
        bruning,
        pleuritic
    }
    public enum eOnsetOfPain
    {
        with_exertion,
        atrest,
        asleep
    }
    public enum eDurationOfTheLastEpisode
    {
        less_then_five,
        five_to_thirty,
        thirty_to_sixty,
        hour_to_six_hours,
        six_hours_to_twelve_hours,
        grater_than_twelve_hours
    }
    struct sPain
    {
        public ePainLocation m_pain_location;
        public ePainChestRadiation m_pain_chest_radiation;
        public ePainCharacter m_pain_character;
        public int m_number_of_hours_since_onset;
        public eDurationOfTheLastEpisode m_duration_of_last_episode;
        public eOnsetOfPain m_onset_of_pain;
    }
}
