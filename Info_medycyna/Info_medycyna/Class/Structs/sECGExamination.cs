﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Info_medycyna.Class.Structs
{
    public struct sECGExamination
    {
        public bool m_new_Q_wave;
        public bool m_any_Q_wave;
        public bool m_new_ST_segment_elevation;
        public bool m_any_ST_segment_elevation;
        public bool m_new_ST_segment_depression;
        public bool m_any_ST_segment_depression;
        public bool m_new_T_wave_inversion;
        public bool m_any_T_wave_inversion;
        public bool m_new_interaventricular_conduction_defect;
        public bool m_any_interaventricular_conduction_defect;
    }
}
