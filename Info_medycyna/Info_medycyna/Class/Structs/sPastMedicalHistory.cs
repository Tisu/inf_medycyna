﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Info_medycyna.Class.Structs
{
    struct sPastMedicalHistory
    {
        public bool m_prior_MI;
        public bool m_prior_angina_prectoris;
        public bool m_prior_atypical_chest_pain;
        public bool m_congestive_hear_failure;
        public bool m_peripheral_vascular_disease;
        public bool m_hiatal_hernia;
        public bool m_hypertension;
        public bool m_diabetes;
        public bool m_smoker;

    }
}
