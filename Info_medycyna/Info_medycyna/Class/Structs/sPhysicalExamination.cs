﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Info_medycyna.Class.Structs
{
    public struct sPhysicalExaminations
    {
        public int m_systolic_blood_pressure;
        public int m_diastolic_blood_pressure;
        public int m_heart_rate;
        public int m_respiration_rate;
        public bool m_rales;
        public bool m_cyanosis;
        public bool m_pallor;
        public bool m_systolic_murmur;
        public bool m_diastolic_murmur;
        public bool m_oedema;
        public bool m_s3_gallop;
        public bool m_s4_gallop;
        public bool m_chest_wall_tenderness;
        public bool m_diaphoresis;
    }    
}
