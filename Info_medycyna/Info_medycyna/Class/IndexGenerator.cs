﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Info_medycyna.Class
{
    class IndexGenerator
    {
        // private readonly int m_iteration;
        private int m_method_calls_count;
        private readonly int m_line_length;
        private int m_index;
        public IndexGenerator(int a_interation, int a_line_length)
        {

            m_line_length = a_line_length;
            m_index = a_interation;

        }
        public int GetIndex()
        {
            int return_value = m_index;
            m_index += m_line_length;
            return return_value;
        }
    }
}
