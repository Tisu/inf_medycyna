﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Info_medycyna.Class
{
    class OpenFile
    {

        public OpenFile()
        {
            Stream stream = null;
            OpenFileDialog open_file_dialog = new OpenFileDialog();

            //open_file_dialog.InitialDirectory = "d:\\";
            open_file_dialog.InitialDirectory = @"D:\GIT\info_medycyna\inf w medycynie\";
            open_file_dialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            open_file_dialog.FilterIndex = 2;
            open_file_dialog.RestoreDirectory = true;
            open_file_dialog.ReadOnlyChecked = true;
            PatientData[] patient_data = new PatientData[60];
            if (open_file_dialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((stream = open_file_dialog.OpenFile()) == null)
                    {
                        return;
                    }
                    using (stream)
                    {                      
                        using (StreamReader stream_reader = new StreamReader(stream))
                        {
                            string buffor = stream_reader.ReadToEnd();                           
                            new ReadData(buffor);
                        }
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }
    }
}
