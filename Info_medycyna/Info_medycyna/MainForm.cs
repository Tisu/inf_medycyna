﻿using Info_medycyna.Class;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Accord.MachineLearning;

namespace Info_medycyna
{
    public partial class MainForm : Form
    {
        const int DEFAULT_NEIGHBORS = 1;
        LoadDataFromFileManager m_load_data_manager = new LoadDataFromFileManager();
        public MainForm()
        {
            InitializeComponent();
            SetOpenFileButtonVisible(m_load_data_manager.m_additional_opening_file_neccessary);
        }
        private void SetOpenFileButtonVisible(bool a_need_to_load_files)
        {
            OpenFiles.Visible = a_need_to_load_files;
            NearestNeighbors.Visible = !a_need_to_load_files;
            NearestMean.Visible = !a_need_to_load_files;
            if (a_need_to_load_files)
            {
                m_load_data_manager.ShowNotLoadedFiles();
            }
        }

        private void NearestNeighbors_Click(object sender, EventArgs e)
        {
            PatientData[] a_patient_Data = m_load_data_manager.GetPatientDataDictionary().First().Value.ToArray();
            int[] output = new int[a_patient_Data.Length];
            int k_nearest_neighbors = ParseNearestNeighborsData();          
            KNearestNeighbors<PatientData> a = new KNearestNeighbors<PatientData>(k_nearest_neighbors, a_patient_Data, output, PatientData.CalculateDistance);           
        }


        private void NearestMean_Click(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void OpenFiles_Click(object sender, EventArgs e)
        {
            SetOpenFileButtonVisible(m_load_data_manager.LoadRemainingFiles());
        }       
        private int ParseNearestNeighborsData()
        {
            int parse_int = 0;
            if (int.TryParse(NearestNeighborsData.Text, out parse_int))
            {
                return parse_int > 0 ? parse_int : DEFAULT_NEIGHBORS;
            }
            return DEFAULT_NEIGHBORS;
        }
    }
}
